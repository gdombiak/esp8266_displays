# ESP8266 AND 20x4 HD44780 LCD

Info on how to hook up your HD44780 LCD to an ESP8266 via i2c.

## Hardware

* [20x4 HD44780 LCD](https://www.amazon.com/JahyShow-Serial-Backlight-Arduino-MEGA2560/dp/B01L8ZCRE2)
* [NodeMCU ESP8266](https://www.ebay.com/itm/Wireless-module-NodeMcu-Lua-WIFI-development-board-based-ESP8266-CP2102/152761175208?ssPageName=STRK%3AMEBIDX%3AIT&_trksid=p2057872.m2749.l2649)

## Wiring

We will use standard SPI pins on the ESP8266.

| HD44780 | ESP8266                                                 |
|---------|---------------------------------------------------------|
| SCL     | GPIO05 (D1)                                             |
| SDA     | GPIO04 (D2)                                             |
| VCC     | 5V (Breadboard Power Supply Module)                     |
| GND     | GND (common ground with Breadboard Power Supply Module) |

### Sources

* [http://www.instructables.com/](http://www.instructables.com/id/I2C-LCD-on-NodeMCU-V2-With-Arduino-IDE/)

## Libraries

* [Arduino-LiquidCrystal-I2C-library](https://github.com/fdebrabander/Arduino-LiquidCrystal-I2C-library)

Copy or clone [Arduino-LiquidCrystal-I2C-library](https://github.com/fdebrabander/Arduino-LiquidCrystal-I2C-library) under Arduino's _libraries_ folder. Restart Arduino once library has been added.

## Software setup

1. Make sure to add `Wire.begin(4, 5);` in setup() before lcd.begin();
1. Update size of LCD to 20x4. Example `lcd(0x27, 20, 4);`

You are now ready to test things. Run any of [these examples](https://github.com/fdebrabander/Arduino-LiquidCrystal-I2C-library/tree/master/examples).
