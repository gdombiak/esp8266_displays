# ESP8266 AND 1.8" SPI TFT 128x160 LCD Module Breakout ST7735R

Info on how to hook up your ST7735 module to an ESP8266.

## Hardware

* [ST7735 module](https://www.aliexpress.com/item/1-8-SPI-TFT-128-x-160-Pixels-Display-LCD-Module-Breakout-ST7735R-Z17-Drop-ship/32810524510.html?spm=a2g0s.9042311.0.0.08DeLv)
* [NodeMCU ESP8266](https://www.ebay.com/itm/Wireless-module-NodeMcu-Lua-WIFI-development-board-based-ESP8266-CP2102/152761175208?ssPageName=STRK%3AMEBIDX%3AIT&_trksid=p2057872.m2749.l2649)

## Wiring

We will use standard SPI pins on the ESP8266.

| TFT                        | ESP8266     |
|----------------------------|-------------|
| LED-                       | GND         |
| LED+                       | 3.3V        |
| CS  (Slave Select)         | GPIO15 (D8) |
| SCL (Serial Clock)         | GPIO14 (D5) |
| SDA (Master Output - MOSI) | GPIO13 (D7) |
| A0  (DC)                   | GPIO 0 (D3) |
| RESET                      | GPIO 2 (D4) |
| VCC                        | 3.3V        |
| GND                        | GND         |

### Sources

* [NodeMCU docs](https://nodemcu.readthedocs.io/en/master/en/modules/spi/)
* [Arduino Forum](http://forum.arduino.cc/index.php?topic=198881.0)

## Libraries

* [TFT_eSPI](https://github.com/Bodmer/TFT_eSPI)

Copy or clone [TFT_eSPI](https://github.com/Bodmer/TFT_eSPI) under Arduino's _libraries_ folder. Restart Arduino once library has been added.

## Software setup

You will need to make a simple change to the TFT_eSPI library to indicate which type of TFT you are using.

1. Edit User_Setup_Select.h
1. Comment out `#include <User_Setup.h>`
1. Uncomment `#include <User_Setups/Setup2_ST7735.h>`
1. Save the file

You are now ready to test things. Run any of [these examples](https://github.com/Bodmer/TFT_eSPI/tree/master/examples/160%20x%20128).

There is [this useful example](https://github.com/Bodmer/TFT_eSPI/tree/master/examples/Test%20and%20diagnostics/Read_User_Setup) that will help you see which pins the library will read from.
