# ESP8266 AND OSEPP 16×2 LCD Display & Keypad Shield

Info on how to hook up your 16×2 LCD Display & Keypad Shield to an ESP8266.

## Hardware

Keep in mind that the _OSEPP 16×2 LCD Display & Keypad Shield_ was designed as an Arduino shield and we will use this information when wiring. It works fine with the ESP8266 or other micro-controllers.

* [OSEPP 16×2 LCD Display & Keypad Shield](https://www.osepp.com/electronic-modules/shields/45-16-2-lcd-display-keypad-shield)
* [NodeMCU ESP8266](https://www.ebay.com/itm/Wireless-module-NodeMcu-Lua-WIFI-development-board-based-ESP8266-CP2102/152761175208?ssPageName=STRK%3AMEBIDX%3AIT&_trksid=p2057872.m2749.l2649)

## Wiring

Look at the Arduino pinout below to understand which pin is what in the shield.

![Arduino pinout](arduino_pinout.jpg)

The column _16X2SHD-01_ uses the names of the Arduino pins.

| 16X2SHD-01 | ESP8266                                                 |
|------------|---------------------------------------------------------|
| RESET      | RST                                                     |
| 5V         | 5V (Breadboard Power Supply Module)                     |
| GND        | GND (common ground with Breadboard Power Supply Module) |
| A0         | A0                                                      |
| 4          | GPIO05 (D1)                                             |
| 5          | GPIO04 (D2)                                             |
| 6          | GPIO00 (D3)                                             |
| 7          | GPIO02 (D4)                                             |
| 8          | GPIO14 (D5)                                             |
| 9          | GPIO12 (D6)                                             |

Notes:

  * A0 is used for reading which button was pressed
    * RESET button will restart the ESP8266
    * SELECT button is read by A0 with a value of 1022/1023 but sometimes 1024 thus does not work reliably
  * If you use other GPIOs on the ESP8266 then update the example.
    * See `LiquidCrystal lcd(14, 12, 5, 4, 0, 2);`

### Sources

* [Arduino Blog](https://create.arduino.cc/projecthub/niftyjoeman/osepp-lcd-and-keypad-shield-d5b46e)

## Libraries

Seems like Arduino ESP8266 does not come with a `LiquidCrystal.h` so we can copy it from [Arduino-LiquidCrystal-I2C-library](https://bitbucket.org/fmalpartida/new-liquidcrystal/src/2e655c19d3e31f6f1bec7952c268733d5812d27c?at=integration).
The provided example copied the necessary code from that repo.


## Software setup

Run example `osepp_16x2_lcd_shield.ino`.
