//Sample using LiquidCrystal library
#include "NewLiquidCrystal.h"

// select the pins used on the LCD panel
LiquidCrystal lcd(14, 12, 5, 4, 0, 2);

// define some values used by the panel and buttons
int lcd_key     = 0;
int adc_key_in  = 0;
#define btnRIGHT  0
#define btnUP     1
#define btnDOWN   2
#define btnLEFT   3
#define btnSELECT 4
#define btnNONE   5

// read the buttons
int read_LCD_buttons()
{
  adc_key_in = analogRead(A0);      // read the value from the sensor

//  if (adc_key_in != 1024) {
//    // Print value when button is pressed
//    Serial.println(adc_key_in);
//  }

// 663 -> LEFT 
// 175 -> UP
// 424 -> DOWN
// 10 -> RIGHT
// 1022 -> SELECT

  // we add approx 50 to those values and check to see if we are close
  if (adc_key_in > 1023) return btnNONE; // We make this the 1st option for speed reasons since it will be the most likely result
  if (adc_key_in < 60)   return btnRIGHT;
  if (adc_key_in < 235)  return btnUP;
  if (adc_key_in < 454)  return btnDOWN;
  if (adc_key_in < 703)  return btnLEFT;
  if (adc_key_in > 1020)  return btnSELECT;
  return btnNONE;  // when all others fail, return this...
}

void setup()
{

  Serial.begin(115200);
  
  lcd.begin(16, 2);              // start the library
  lcd.setCursor(0, 0);
  lcd.print("Push the buttons"); // print a simple message
}

void loop()
{
  lcd.setCursor(9, 1);           // move cursor to second line "1" and 9 spaces over
  lcd.print(millis() / 1000);    // display seconds elapsed since power-up


  lcd.setCursor(0, 1);           // move to the begining of the second line
  lcd_key = read_LCD_buttons();  // read the buttons

  switch (lcd_key)               // depending on which button was pushed, we perform an action
  {
    case btnRIGHT:
      {
        lcd.print("RIGHT ");
        break;
      }
    case btnLEFT:
      {
        lcd.print("LEFT   ");
        break;
      }
    case btnUP:
      {
        lcd.print("UP    ");
        break;
      }
    case btnDOWN:
      {
        lcd.print("DOWN  ");
        break;
      }
    case btnSELECT:
      {
        lcd.print("SELECT");
        break;
      }
    case btnNONE:
      {
        lcd.print("NONE  ");
        break;
      }
  }

}
