# Examples of different displays hooked up to an ESP8266

## SPI

* [RPi 3.5 Inch 480×320 TFT Touch Screen Monitor](spi_RPI_ILI9486_tft/README.md)
* [1.8" SPI TFT 128x160 LCD Module Breakout ST7735](spi_st7735_tft/README.md)

## i2c

* [20x4 HD44780 LCD](i2c_HD44780_LCD/README.md)

## Direct 4 bits

* [OSEPP 16×2 LCD Display & Keypad Shield](osepp_16x2_lcd_shield/README.md)
