# ESP8266 AND RPi 3.5 Inch 480×320 TFT Touch Screen Monitor

Info on how to hook up your RPi ILI9486 TFT to an ESP8266.

## Hardware

* [RPi Elegoo 3.5 Inch 480×320 TFT Touch Screen Monitor](https://www.amazon.com/gp/product/B01N3JROH8) or [From Elegoo.com](https://www.elegoo.com/product/elegoo-3-5-inch-480x320-tft-touch-screen-monitor/)
* [NodeMCU ESP8266](https://www.ebay.com/itm/Wireless-module-NodeMcu-Lua-WIFI-development-board-based-ESP8266-CP2102/152761175208?ssPageName=STRK%3AMEBIDX%3AIT&_trksid=p2057872.m2749.l2649)

## Wiring

We will use standard SPI pins on the ESP8266.

| RPi TFT LCD                | ESP8266                             | ESP32         |
|----------------------------|-------------------------------------|---------------|
| CS  (Slave Select)         | GPIO15 (D8)                         | GPIO 15 (D15) |
| SCK (Serial Clock)         | GPIO14 (D5)                         | GPIO 18 (D18) |
| MOSI                       | GPIO13 (D7)                         | GPIO 23 (D23) |
| DC                         | GPIO 0 (D3)                         | GPIO 2 (D2)   |
| RST                        | GPIO 2 (D4)                         | GPIO 4 (D4)   |
| VCC                        | 5V (Breadboard Power Supply Module) | same |
| GND                        | GND                                 | same |

I'm using a [Breadboard Power Supply Module MB102 3.3V 5V](https://www.amazon.com/JBtek-Breadboard-Supply-Arduino-Solderless/dp/B010UJFVTU) that can supply 5V and/or 3.3V to a breadboard. In this case, I'm using 5V to power the RPi TFT LCD. Ground is connected to GND of ESP8266 and to the GND of the RPi TFT LCD. The esp8266 is being powered from the USB in this example.

### Sources

* [NodeMCU docs](https://nodemcu.readthedocs.io/en/master/en/modules/spi/)

![TFT Pins](https://github.com/Bodmer/TFT_eSPI/raw/master/Tools/RPi_TFT_Connections.png)

## Libraries

* [TFT_eSPI](https://github.com/Bodmer/TFT_eSPI)

Copy or clone [TFT_eSPI](https://github.com/Bodmer/TFT_eSPI) under Arduino's _libraries_ folder. Restart Arduino once library has been added.

## Software setup

You will need to make a simple change to the TFT_eSPI library to indicate which type of TFT you are using.

1. Edit User_Setup_Select.h
1. Comment out `#include <User_Setup.h>`
1. Uncomment `#include <User_Setups/Setup10_RPi_touch_ILI9486.h>`
    1. For ESP32 use `#include <User_Setups/Setup11_RPi_touch_ILI9486.h> `
1. Save the file

You are now ready to test things. Run any of [these examples](https://github.com/Bodmer/TFT_eSPI/tree/master/examples/480%20x%20320).

There is [this useful example](https://github.com/Bodmer/TFT_eSPI/tree/master/examples/Test%20and%20diagnostics/Read_User_Setup) that will help you see which pins the library will read from.
